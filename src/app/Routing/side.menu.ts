import { SideMenu } from "../Models/VmModels/SideMenu";

export class SideMenuList {
    public listItems : SideMenu[] = [
        { title : 'Dashboard', icon : 'dashboard' , routeUrl: '/home' },
        { title : 'Clients', icon : 'people' , routeUrl: '/client' },
        { title : 'Api Resources', icon : 'http' , routeUrl: '/api-resources' },
        { title : 'Identity Resources', icon : 'security' , routeUrl: '/identity-resources' },
        { title : 'Users', icon : 'account_circle' , routeUrl: '/users' },
        { title : 'Roles', icon : 'verified_user' , routeUrl: '/roles' },
        { title : 'User Claims', icon : 'contacts' , routeUrl: '/user-claims' },
    ]
}