import { UserClaimsComponent } from './../Components/Main/user-claims/user-claims.component';
import { UsersComponent } from './../Components/Main/users/users.component';
import { IdentityResourcesComponent } from './../Components/Main/identity-resources/identity-resources.component';
import { ApiResourcesComponent } from './../Components/Main/api-resources/api-resources.component';
import { DashboardComponent } from './../Components/Shared/dashboard/dashboard.component';
import { HomeComponent } from './../Components/Main/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { RolesComponent } from '../Components/Main/roles/roles.component';


const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '', component: DashboardComponent, children : [
        {path : 'home' , component : HomeComponent},
         {path : 'client' , loadChildren : '../Components/Main/clients/client.module#ClientModule'},
        {path : 'api-resources' , component : ApiResourcesComponent},
        {path : 'identity-resources' , component : IdentityResourcesComponent},
        {path : 'users' , component : UsersComponent},
        {path : 'roles' , component : RolesComponent},
        {path : 'user-claims' , component : UserClaimsComponent}
    ]},

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }