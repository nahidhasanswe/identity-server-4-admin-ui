import { ClientModule } from './Components/Main/clients/client.module';
import { AppRoutingModule } from './Routing/app.routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './Components/Main/home/home.component';
import { SideBarComponent } from './Components/Shared/side-bar/side-bar.component';
import { NavBarComponent } from './Components/Shared/nav-bar/nav-bar.component';
import { DashboardComponent } from './Components/Shared/dashboard/dashboard.component';
import { LoginComponent } from './Components/Auth/login/login.component';
import { FooterComponent } from './Components/Shared/footer/footer.component';
import { PageHeaderComponent } from './Components/Shared/page-header/page-header.component';
import { PageHeaderService } from './Common/Service/page.header.service';
import { UsersComponent } from './Components/Main/users/users.component';
import { RolesComponent } from './Components/Main/roles/roles.component';
import { UserClaimsComponent } from './Components/Main/user-claims/user-claims.component';
import { IdentityResourcesComponent } from './Components/Main/identity-resources/identity-resources.component';
import { ApiResourcesComponent } from './Components/Main/api-resources/api-resources.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SideBarComponent,
    NavBarComponent,
    DashboardComponent,
    LoginComponent,
    FooterComponent,
    PageHeaderComponent,
    UsersComponent,
    RolesComponent,
    UserClaimsComponent,
    IdentityResourcesComponent,
    ApiResourcesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule
  ],
  providers: [PageHeaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
