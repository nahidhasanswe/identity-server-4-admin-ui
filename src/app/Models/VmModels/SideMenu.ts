export class SideMenu implements ISideMenu {
    public title: string;    
    public routeUrl: string;
    public icon: string;
}

export interface ISideMenu {
    title : string;
    routeUrl: string;
    icon : string;
}