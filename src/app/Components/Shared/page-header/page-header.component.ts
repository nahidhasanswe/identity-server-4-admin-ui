import { Component, OnInit } from '@angular/core';
import { PageHeaderService } from '../../../Common/Service/page.header.service';

@Component({
  selector: 'page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent implements OnInit {

  public title: string = 'Home';
  public subtitle : string = 'Dashboard';

  constructor(public pageHeader : PageHeaderService) { }

  ngOnInit() {
    this.pageHeader.obserable.subscribe(response => {
      this.title = response.title;
      this.subtitle = response.subtitle;
    })
  }

}
