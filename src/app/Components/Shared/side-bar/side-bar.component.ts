import { SideMenuList } from './../../../Routing/side.menu';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'page-sidebar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  public sideMenuList = new SideMenuList();
  
  constructor() { }

  ngOnInit() {
  }

}
