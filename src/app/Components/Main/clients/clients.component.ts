import { AddClientComponent } from './add-client/add-client.component';
import { PageHeaderService } from './../../../Common/Service/page.header.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  constructor(
    private pageHeader : PageHeaderService,
    private dialog : MatDialog) { }

  ngOnInit() {
    this.setPageHeader();
  }

  private setPageHeader()
  {
    this.pageHeader.setPageHeader('Clients Setup', 'Identity Server Client');
  }

  public AddClient() {
    const dialogRef = this.dialog.open(AddClientComponent, {
      width: '700px',
      minWidth: '400px',
      closeOnNavigation : true
    });
  }

}

export class VmClient {
  ClientId : string;
  ClientName: string;
  Description: string;
  ClientUri : string;
  LogoUri : string;
}
