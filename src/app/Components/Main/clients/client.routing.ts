import { ClientConfigComponent } from './client-config/client-config.component';
import { ClientsComponent } from './clients.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'client-list', pathMatch:'full' },
    { path: 'client-list' , component: ClientsComponent},
    { path: 'config/:id' , component: ClientConfigComponent},
    { path: '**', redirectTo: 'home' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClientRoutingModule { }