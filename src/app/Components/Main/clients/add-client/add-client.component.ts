import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Guid } from '../../../../Common/Utility/Guid';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { VmClient } from '../clients.component';
import { Router } from '../../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {

  public ClientId : string;

  public clientForm;

  constructor(private dialogRef: MatDialogRef<AddClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private route: Router) {}

  ngOnInit() {
    this.InitialForm();
  }

  private InitialForm() {
    this.clientForm = new FormGroup({
      ClientId : new FormControl('', Validators.required),
      Description : new FormControl('', Validators.required),
      ClientName : new FormControl('', Validators.required),
      ClientUri : new FormControl(''),
      LogoUri : new FormControl('')
    })
  }

  public GenerateGuid() {
    this.clientForm.controls['ClientId'].setValue(Guid.NewGuid());
  }

  public AfterSubmitClient(value: VmClient, isValid : boolean) {
    if(isValid) {
      console.log(value);
    }
  }

  public AfterSubmitAndConfig(value: VmClient, isValid : boolean) {
    if(isValid) {
      this.dialogRef.close();
      this.route.navigate(['client/config', value.ClientId]);
    }
  }

}
