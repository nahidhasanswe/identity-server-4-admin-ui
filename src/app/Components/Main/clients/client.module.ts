import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatDialogModule, MatTabsModule } from '@angular/material';
import { ClientsComponent } from './clients.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClientRoutingModule } from './client.routing';
import { AddClientComponent } from './add-client/add-client.component';
import { ClientConfigComponent } from './client-config/client-config.component';
import { ClientScopesComponent } from './client-scopes/client-scopes.component';
import { ClientGrantTypesComponent } from './client-grant-types/client-grant-types.component';
import { ClientProviderRestrictionComponent } from './client-provider-restriction/client-provider-restriction.component';
import { ClientPropertiesComponent } from './client-properties/client-properties.component';
import { ClientUrisComponent } from './client-uris/client-uris.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ClientRoutingModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatTabsModule
  ],
  declarations: [
    ClientsComponent,
    AddClientComponent,
    ClientConfigComponent,
    ClientScopesComponent,
    ClientGrantTypesComponent,
    ClientProviderRestrictionComponent,
    ClientPropertiesComponent,
    ClientUrisComponent
  ]
  ,
  providers: [
    
  ],
  entryComponents : [
    AddClientComponent
  ]
})
export class ClientModule { }