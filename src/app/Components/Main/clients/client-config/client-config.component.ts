import { PageHeaderService } from './../../../../Common/Service/page.header.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-client-config',
  templateUrl: './client-config.component.html',
  styleUrls: ['./client-config.component.css']
})
export class ClientConfigComponent implements OnInit {

  private clientId : string;

  constructor(
    private activeRoute: ActivatedRoute,
    private pageService: PageHeaderService) { }

  ngOnInit() {
    this.pageService.setPageHeader('Client Configuration','openid auth2');

    this.activeRoute.params.subscribe(param => {
      this.clientId = param.id;
    })
  }

}
