import { PageHeaderService } from './../../../Common/Service/page.header.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-claims',
  templateUrl: './user-claims.component.html',
  styleUrls: ['./user-claims.component.css']
})
export class UserClaimsComponent implements OnInit {

  constructor(private pageHeader : PageHeaderService) { }

  ngOnInit() {
    this.setPageHeader();
  }

  private setPageHeader()
  {
    this.pageHeader.setPageHeader('User Token Claims', 'User Claims');
  }

}
