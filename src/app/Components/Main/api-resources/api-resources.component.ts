import { PageHeaderService } from './../../../Common/Service/page.header.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-api-resources',
  templateUrl: './api-resources.component.html',
  styleUrls: ['./api-resources.component.css']
})
export class ApiResourcesComponent implements OnInit {

  constructor(private pageHeader : PageHeaderService) { }

  ngOnInit() {
    this.setPageHeader();
  }

  private setPageHeader()
  {
    this.pageHeader.setPageHeader('Api Resources Setup', 'Api Resources');
  }

}
