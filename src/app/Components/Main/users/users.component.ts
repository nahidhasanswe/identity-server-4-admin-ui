import { PageHeaderService } from './../../../Common/Service/page.header.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private pageHeader : PageHeaderService) { }

  ngOnInit() {
    this.setPageHeader();
  }

  private setPageHeader()
  {
    this.pageHeader.setPageHeader('Clients User', 'Users');
  }

}
