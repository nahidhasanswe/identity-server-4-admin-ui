import { PageHeaderService } from './../../../Common/Service/page.header.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-identity-resources',
  templateUrl: './identity-resources.component.html',
  styleUrls: ['./identity-resources.component.css']
})
export class IdentityResourcesComponent implements OnInit {

  constructor(private pageHeader : PageHeaderService) { }

  ngOnInit() {
    this.setPageHeader();
  }

  private setPageHeader()
  {
    this.pageHeader.setPageHeader('Identity Resources Setup', 'Identity Resources');
  }

}
