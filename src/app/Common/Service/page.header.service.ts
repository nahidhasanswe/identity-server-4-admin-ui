import { Injectable, EventEmitter } from "@angular/core";
import { Title } from "@angular/platform-browser";


@Injectable()
export class PageHeaderService {

    public obserable  : EventEmitter<any> = new EventEmitter<any>();

    constructor(private pageTitlte : Title) {}

    public setPageHeader(title: string, subTitle: string) {
        const pageHeader = {
            title : title,
            subtitle: subTitle
        };

        this.pageTitlte.setTitle('Identity Server 4 -' +title);
        setTimeout(() => {
            this.obserable.next(pageHeader); 
        });
    }
}